from behave import step
from BDDCommon import bddCommonFunctions
from tests.features import create_users_steps

@step('I have the {resource} API for {API_name}')
def get_API_request(context, resource, API_name):
    context.header = bddCommonFunctions.get_header()
    context.url = bddCommonFunctions.get_url(API_name, resource)
    assert context.url is not None, "the url is None for given API name {} and resource {}".format(API_name, resource)


@step('I send {method} request')
def send_API_request(context, method):
    if method.upper() == 'GET':
        context.response = bddCommonFunctions.make_request(url=context.url, method=method, headers=
                                                           context.header)
    else:
        context.response = bddCommonFunctions.make_request(url=context.url, method=method, headers=context.header,
                                                           payload=context.payload)
    assert context.response is not None, "Error - the response is None"


@step('I should get {resp_code:d} response code')
def verify_response_code(context, resp_code):
    actual_resp_code = context.response.status_code
    result = bddCommonFunctions.verify_statuscode(resp_code, actual_resp_code)
    assert result == True, "Error - The actual status code {} doe snot match with expected response code".format(
        actual_resp_code, resp_code
    )

@step('I should get response text as {expected_response}')
def verify_response_text_contains(context, expected_response):
    actual_response = context.response.text
    result = bddCommonFunctions.verify_response_contains(expected_response, actual_response)
    assert result == True, "Error - the actual response text {} does not match with expected response text".format(
        actual_response, expected_response
    )