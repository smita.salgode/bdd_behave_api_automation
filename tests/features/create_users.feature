Feature: Verify create user API

  Scenario Outline: Validate create valid user
    Given I have the create user API for users_api
    And I create create user json payload with <name>, <job>
    When I send post request
    Then I should get 201 response code
    And I should get response text as createdAt

    Examples:
    | name | job |
    | morpheus | leader |