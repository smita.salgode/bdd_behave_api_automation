from behave import step
from BDDCommon import bddCommonFunctions
from BDDCommon import createJsonPayload


@step('I create create user json payload with {name}, {job}')
def create_user_json_payload(context, name, job):
    context.payload = createJsonPayload.createUsersPayload(name, job)
    assert context.payload is not None, "Error - the payload failed to create"