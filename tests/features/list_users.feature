Feature: Verify the user lists API

  Scenario: Validate the list users API
    Given I have the list users API for users_api
    When I send get request
    Then I should get 200 response code
    And I should get response text as total_pages