class ApiResources:

    API_NAMES ={
        'list users' : 'api/users?page=2',
        'create user': 'api/users',
        'update user': 'api/users/2'
    }