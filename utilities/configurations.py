import configparser
import logging
import os
import inspect


def get_config():
    path = '/'.join((os.path.abspath(__file__).replace("\\", "/")).split("/")[:-1])
    config = configparser.ConfigParser()
    config.read(os.path.join(path, "properties.ini"))

    return config

def get_logger():
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)
    fileHandler = logging.FileHandler("{}.log".format("API_automation"), mode='a')

    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s: %(message)s',
                                  datefmt='%m %d %Y  %I:%S %p')
    fileHandler.setFormatter(formatter)
    logger.addHandler(fileHandler)

    return logger