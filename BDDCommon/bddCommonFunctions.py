from utilities.resources import ApiResources
from utilities.configurations import *
import requests
import traceback
import json
import time
log = get_logger()


def get_url(api_name, resource):
    try:
        log.info("Getting the url for proxy processor {}".format(api_name))
        base_url = get_config()['API'][api_name]
        print(resource, "**************************")
        print(ApiResources.API_NAMES, "&&&&&&&&&&&&&&&&&&&&&&&&&")
        processor = ApiResources.API_NAMES.get(resource)
        url = "https://{}/{}".format(base_url, processor)

    except Exception as e:
        log.error("Failed to create url for API {}".format(api_name))
        log.error(traceback.format_exc())
        url = None
    return url


def get_header():
    headers = None
    try:
        headers = {'content-type':'application/json'}
    except Exception as e:
        log.error(traceback.format_exc())
        log.error("Failed to get header")
    return headers


def make_request(url, method="GET", headers=None, payload=None, params=None):

    response = None
    try:
        if None not in (url, method):
            method = method.upper()
            response = requests.request(method=method, url=url, headers=headers, data=json.dumps(payload))
        else:
            log.error("Either url {}, method {} or params {} is blank".format(url, method,params))
    except Exception as e:
        log.error(traceback.format_exc())
        log.error("The API failed to get response")
    return response


def verify_statuscode(expected_statuscode, actual_statuscode):
    result = False
    try:
        if actual_statuscode == expected_statuscode:
            log.info("Expected status code {} matches with actual status code {}".format(expected_statuscode,
                                                                                         actual_statuscode))
            result = True
        else:
            log.error("Error - Expected status code {} does not match with actual status code {}".format(expected_statuscode,
                                                                                                         actual_statuscode))
    except Exception as e:
        log.error(traceback.format_exc())
        log.error("Error - failed to compare both status code")
    return result


def verify_response_contains(expected_response_text, actual_response_text):
    result = False
    try:
        if None not in (expected_response_text, actual_response_text):
            expected_response_text = expected_response_text.replace('"', '')
            actual_response_text = actual_response_text.replace('"', '')
            if expected_response_text.lower() in actual_response_text.lower():
                result = True
                log.info("The expected test {} is present in response text {}".format(expected_response_text,
                                                                                      actual_response_text))
            else:
                log.error("Error - the expected text {} is not present in the response text {}".format(expected_response_text,
                                                                                                       actual_response_text))
    except Exception as e:
        log.error(traceback.format_exc())
        log.error("Error - failed to compare")
    return result



